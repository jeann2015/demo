<?php
  include ("db.php"); 
  $conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);

  $var_fecha_actual = fecha_aplicacion($conn);
  $hora_aplicacion = hora_aplicacion($conn);

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Demo</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<script type="text/javascript" src="lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="lib/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="lib/jscalendar/calendar-setup.js"></script>
<link href="lib/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css"/>

</head>
<body>

  <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Demo</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              
              
              <!-- <li><a href="#">Contact</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li> -->
            </ul>
            
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
      <form action="horarios.php" method="post">

        <h1>Registro</h1>
        <table class="table">
        
        <tr class="">
          <th colspan="6"><div align="right"><a class="btn btn-primary" role="button" href="index.php">Regresar</a></div></th>
          </tr>
        	<tr class="">
        		<th>Evento</th>
            <th><?php echo select18("id", "nombre", "$id_evento", "select id,nombre from eventos order by id", "evento",1,$conn,""); ?></th>
          </tr>
          <tr>
            <th>Direccion</th>
            <th>
              <select name="direccion" id="direccion"></select>
            </th>
        	</tr>
          <tr>
            <th>Fecha</th>
            <th>
              <select name="fecha" id="fecha"></select>
            </th>
          </tr>
          <tr>
            <th>hora</th>
            <th>
            <select name="hora" id="hora"></select>
            </th>
          </tr>
          <tr>
            <th colspan="2"><button type="submit" class="btn btn-default">Registrarse</button></th>
            <input type="hidden" value="1" name="action" >
          
          </tr>
          
        </table>

        </form>
      </div>

    </div> <!-- /container -->




<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script>
  
$("#evento").change(function(){
      
      var evento = $("#evento").val();
      $.ajax({
        url   : "funciones.php?accion=1&evento=" + evento,
        type  :  'get',
      }).done(function(result){
        var options = $("#direccion");
        options.empty();
        $.each(result.domains, function() {
            options.append($("<option />").val(this.id).text(this.name));
        });
      });

    });

</script>
</body>
</html>