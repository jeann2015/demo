<?php
  include ("db.php"); 
  $conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);

  $var_fecha_actual = fecha_aplicacion($conn);
  $hora_aplicacion = hora_aplicacion($conn);

$id = $_GET['id'];

$sSql="select h.id,h.id_evento,h.id_direccion,h.fecha,h.hora from horarios h,direccion d,eventos e where 
h.id_evento = e.id and h.id_direccion = d.id and h.id = $id order by h.id";
            
$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
while ($row_rs = $rs->fetch_assoc())
{
  $id = $row_rs['id'];
  $id_evento = $row_rs['id_evento'];
  $id_direccion = $row_rs['id_direccion'];
  $fecha = $row_rs['fecha'];
  $hora =  $row_rs['hora'];
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Demo</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<script type="text/javascript" src="lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="lib/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="lib/jscalendar/calendar-setup.js"></script>
<link href="lib/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css"/>

</head>
<body>

  <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Demo</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              
              
              <!-- <li><a href="#">Contact</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li> -->
            </ul>
            
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
      <form action="horarios.php" method="post">

        <h1>Modificar Horarios</h1>
        <table class="table">
        
        <tr class="">
          <th colspan="6"><div align="right"><a class="btn btn-primary" role="button" href="administrador.php">Regresar</a></div></th>
          </tr>
        	<tr class="">
        		<th>Evento</th>
            <th><?php echo select("id", "nombre", $id_evento, "select id,nombre from eventos order by id", "evento",1,$conn) ?></th>
          </tr>
          <tr>
            <th>Direccion</th>
            <th><?php echo select("id", "nombre", $id_direccion, "select id,nombre from direccion order by id", "direccion",1,$conn) ?></th>
        	</tr>
          <tr>
            <th>Fecha</th>
            <th>
              <input class="input-block-level"  required='true' name="fecha" id="fecha" type="text" class="textbox" 
              value="<?php echo fecha($fecha); ?>" size="15" maxlength="20" />
              <input name="image7" type="image" id="image7" src="lib/jscalendar/cal.gif" />
              <script type="text/javascript"> Calendar.setup( {inputField:"fecha",ifFormat:"%d/%m/%Y",button:"image7",firstDay:1,weekNumbers:false,showOthers:true} );</script>
            </th>
          </tr>
          <tr>
            <th>hora</th>
            <th>
              <input class="input-block-level"  required='true' name="hora" id="hora" type="text" class="textbox" 
              value="<?php echo conversion_hora($hora); ?>" size="15" maxlength="20" />
              <input name="image7" type="image" id="image8" src="lib/jscalendar/cal.gif" />
              <script type="text/javascript"> Calendar.setup( {inputField:"hora",ifFormat:"%l:%M:%S %p",button:"image8",firstDay:1,weekNumbers:false,showOthers:true,showsTime:true} );</script>
            </th>
          </tr>
          <tr>
            <th colspan="2"><button type="submit" class="btn btn-default">Modificar</button></th>
            <input type="hidden" value="2" name="action" >
            <input type="hidden" value="<?php echo $id; ?>" name="id" >
          
          </tr>
          
        </table>

        </form>
      </div>

    </div> <!-- /container -->




<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>