<?php
  include ("db.php"); 
  $conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Demo</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>

  <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Demo Jean Carlos Nunez, bitbugsoluciones</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="administrador.php">Principal</a></li>
              
              
              <!-- <li><a href="#">Contact</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li> -->
            </ul>
            
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
      <form action="direccion_view.php" method="post">

        <h1>Direccion</h1>
        <table class="table">
        <tr>
        <td >
          <select class="form-control" name="buscar" id="buscar">
            <option value="nombre">Nombre</option>
            <option value="ID">ID</option>
          </select>
        </td>
        <td colspan=""> <input type="text" class="form-control"> </td>
        <td colspan="5"> <button type="submit" class="btn btn-primary">Buscar</button></td>
        </tr>
        <tr class="">
          <th colspan="5"><div align="center"><a class="btn btn-primary" role="button" href="direccion_add.php">Ingresar</a></div></th>
          </tr>
        	<tr class="info">
        		<th>Id</th>
            <th>Nombre</th>
            <th>Modificar</th>
            <th>Eliminar</th>
        	</tr>
          <?php
            $sSql="select * from direccion e order by e.id";
            
            $rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
            while ($row_rs = $rs->fetch_assoc())
            {
              $id = $row_rs['id'];
              $nombre_evento = $row_rs['nombre'];
              
            
          ?>
          <tr>
            <th><?php echo $id; ?></th>
            <th><?php echo $nombre_evento; ?></th>
            <th>
            <a href="direccion_mod.php?id=<?php echo $id; ?>" class="btn btn-primary ">
            <span class="glyphicon glyphicon-pencil"></span> 
            </a>
            </th>
            <th>
              <a href="direccion_eli.php?id=<?php echo $id; ?>" class="btn btn-primary ">
            <span class="glyphicon glyphicon-remove"></span> 
            </a>
            </th>
            
          </tr>

          <?php } ?>
        </table>

        </form>
      </div>

    </div> <!-- /container -->

<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>